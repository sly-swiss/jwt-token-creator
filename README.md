#Tool to generate JWT Token for Zitadel service users

## Build
`./gradlew clean shadowjar`

## Run
```
java -jar tokencreator/build/libs/tokencreator-1.0.1-all.jar --text '{....}'
```
or
```
`java -jar tokencreator/build/libs/tokencreator-1.0.1-all.jar --file pathToFile
```